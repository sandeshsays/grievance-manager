$('#province_id').on('change', function () {
    var province_id = $(this).val();
    var token = $('meta[name="csrf-token"]').attr('content');
    $.post(site_url + '/districtByCode', {
        province_id: province_id, _token: token
    }, function (status) {
        $('#district_id').prop('disabled',false);
        $('#district_id').html(status);
    });

})
$('#district_id').on('change', function () {
    var district_id = $(this).val();
    $.post(site_url + '/localBodyByCode', {
        district_id: district_id, _token: $('meta[name="csrf-token"]').attr('content')
    }, function (status) {
        $('#client_id').prop('disabled',false);
        $('#client_id').html(status);
    });

})