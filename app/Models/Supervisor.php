<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supervisor extends Model
{
    use HasFactory;

    protected $fillable = [
        'local_govt_code',
        'username',
        'passkey',
        'full_name',
        'contact_no',
        'email',
        'hh_form',
        'status',
    ];
}
