<?php

return[
    'app_name' => 'Grievance Management System',
    'home' => 'Home',
    'faq' => 'Faq',
    'privacy_policy' => 'Privacy Policy',
    'suggestion' => 'Suggestion',
    'contact' => 'Contact',
    'filter' => 'Filter',
    'login' => 'Login',
    'details' => 'Details',
    'suggestion_text' => 'Give some suggestions to improve the system for the utter benefits of Nepali Citizens.',
];
