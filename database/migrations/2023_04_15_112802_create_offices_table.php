<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offices', function (Blueprint $table) {
            $table->id();
            $table->string('code',255);
            $table->string('name',255);
            $table->string('name_ne',255);
            $table->string('logo',255);
            $table->text('description');
            $table->string('ward',255);
            $table->string('street',255);
            $table->unsignedBigInteger('office_category_id');
            $table->foreign('office_category_id')->references('id')->on('office_categories');
            $table->unsignedBigInteger('province_id');
            $table->foreign('province_id')->references('id')->on('province');
            $table->unsignedBigInteger('district_id');
            $table->foreign('district_id')->references('id')->on('district');
            $table->unsignedBigInteger('local_govt_id');
            $table->foreign('local_govt_id')->references('id')->on('local_government');
            $table->boolean('status')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offices');
    }
}
