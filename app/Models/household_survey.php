<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class household_survey extends Model
{
    use HasFactory;

    protected $table = 'household_survey';

    protected $fillable = [
        'start',
        'end',
        'username',
        'deviceid',
        'geopoint',
        'full_name_np',
        'full_name_block',
        'photo',
        'father',
        'mother',
        'grandfather',
        'grandmother',
        'province',
        'district',
        'localbody',
        'ward',
        'plus_code',
        'device',
        'devicecode',
        'auto_house_no',
        'street',
        'tole_dev_institution',
        'phone',
        'family_member_count'

    ];
}
