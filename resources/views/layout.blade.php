<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>GHS</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{ asset('assets/img/favicon.png') }}" rel="icon">
  <link href="{{ asset('assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

  <!-- Add icon library -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/glightbox/css/glightbox.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{ asset('assets/css/style_landing.css') }}" rel="stylesheet">

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center justify-content-between">

    <a href="/" class="logo"><img src="{{ asset('assets/images/logo.png') }}" alt="" class="img-fluid"></a>
      <h1 class="logo"><a href="/">{{ __('frontend.app_name') }}</a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->

      <nav id="navbar" class="navbar">
        <ul>
            <li><a class="nav-link scrollto active" href="/">{{ __('frontend.home') }}</a></li>
            <li><a class="nav-link scrollto" href="#about">{{ __('frontend.faq') }}</a></li>
            <li><a class="nav-link scrollto" href="#">{{ __('frontend.privacy_policy') }}</a></li>
            <li><a class="nav-link scrollto " href="{{ route('suggestions.index') }}">{{ __('frontend.suggestion') }}</a></li>
            <li><a class="nav-link scrollto" href="#contacts">{{ __('frontend.contact') }}</a></li>
            <li><a class="getstarted scrollto" href="/admin">{{ __('frontend.login') }}</a></li>

            <li><a class="nav-link scrollto" href="#">
                <select class="changeLang">
                    <option value="ne" {{ session()->get('locale') == 'ne' ? 'selected' : '' }}>नेपाली</option>
                    <option value="en" {{ session()->get('locale') == 'en' ? 'selected' : '' }}>English</option>
                </select></a>
            </li>


        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center">

    <div class="container">
        <div class="row">
          <div class="col-lg-6 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center">
            <h1>{{ __('frontend.suggestion') }}</h1>
            <h2>{{ __('frontend.suggestion_text') }}
            </h2>
            <div class="d-flex">
              <a href="{{ route('suggestions.create') }}" class="btn-get-started scrollto"> {{ __('frontend.get started') }}</a>
            </div>
          </div>
          <div class="col-lg-6 order-1 order-lg-2 hero-img">
            <img src="{{ asset('assets/img/hero-img.png') }}" class="img-fluid animated" alt="">
          </div>
        </div>
    </div>

</section><!-- End Hero -->

  <main id="main">
    <div class="container">
        @yield('content')
    </div>

    <!-- ======= Contacts Section ======= -->
  <section id="contacts" class="testimonials section-bg">
    <div class="container">
        <div class="text-center">
            <h3>{{ __('frontend.contact') }} {{ __('frontend.details') }}</h3>
            <p>Et aut eum quis fuga eos sunt ipsa nihil. Labore corporis magni eligendi fuga maxime saepe commodi placeat.</p>
        </div>
    </div>
</section><!-- End Contacts Section -->

  </main>


  <!-- ======= Footer ======= -->
  <footer id="footer">
    {{-- <section id="contacts">
        <div class="footer-top">
            <div class="container">
            <div class="row  justify-content-center">
                <div class="col-lg-6">
                <h3>{{ __('frontend.contact') }} {{ __('frontend.details') }}</h3>
                <p>Et aut eum quis fuga eos sunt ipsa nihil. Labore corporis magni eligendi fuga maxime saepe commodi placeat.</p>
                </div>
            </div>
            </div>
        </div>
    </section> --}}

    <div class="container footer-bottom clearfix">
      <div class="copyright">
        &copy; Copyright <strong><span>eNno</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/enno-free-simple-bootstrap-template/ -->
        <!--Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a> -->
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<!-- Vendor JS Files -->
<script src="{{ asset('assets/vendor/purecounter/purecounter_vanilla.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/vendor/glightbox/js/glightbox.min.js') }}"></script>
<script src="{{ asset('assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('assets/vendor/swiper/swiper-bundle.min.js') }}"></script>
<script src="{{ asset('assets/vendor/php-email-form/validate.js') }}"></script>

<!-- Template Main JS File -->
<script src="{{ asset('assets/js/main.js') }}"></script>

<script>
  var url = "{{ route('lan.change') }}";

  $('.changeLang').change(function(){
      window.location.href = url + "?lang=" + $(this).val();
  });
</script>

</body>

</html>
