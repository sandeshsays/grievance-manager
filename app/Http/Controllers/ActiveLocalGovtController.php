<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ActiveLocalGovtController extends Controller
{
    public function index()
    {
        $local_govt = DB::table('vw_active_local_government')->paginate(10);

        return view('active_local_govt', compact('local_govt'));
    }
}
