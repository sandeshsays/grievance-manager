@extends('layout')

@section('content')

<section id="services" class="services section-bg">
    <div class="container">
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add New Suggestion</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('suggestions.index') }}"><i class="fa fa-arrow-left"></i> {{ __('frontend.back') }}</a>
        </div>
    </div>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="row">
<div class="col-lg-12 mt-5 mt-lg-0 d-flex align-items-stretch">

    <form action="{{ route('suggestions.store') }}" method="POST" enctype="multipart/form-data">
        @csrf

         <div class="row">
                <div class="form-group col-md-6 mt-3 mt-md-0">
                    <strong>Name:</strong>
                    <input type="text" name="name" class="form-control" placeholder="Name">
                </div>
                <div class="form-group col-md-6 mt-3 mt-md-0">
                    <strong>Mobile Number:</strong>
                    <input type="text" class="form-control" name="mobile" placeholder="Mobile">
                </div>
                <div class="form-group">
                    <strong>Suggestion:</strong>
                    <textarea class="form-control" name="suggestion"></textarea>
                </div>
                <div class="form-group">
                    <strong>File:</strong>
                    <input type="file" name="files" class="form-control" placeholder="image">
                </div>
                {{-- <div class="text-center"><button type="submit">Submit</button></div> --}}
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary"><i class="fa fa-arrow-floppy-o"></i> {{ __('frontend.save') }}</button>
            </div>

        </div>

    </form>

  </div>
</div>
    </div>

</section>

  <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
       $('.ckeditor').ckeditor();
    });
</script>

@endsection
