<?php

namespace App\Http\Controllers;

use crocodicstudio\crudbooster\helpers\CRUDBooster;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function admin_dashboard()
    {
        $data = [];
        $data['page_title'] = 'Dashboard';
        return view('admin_dashboard', $data);
    }


}
