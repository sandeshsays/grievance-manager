<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FormTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(DB::table('form_types')->count() == 0){
            DB::table('form_types')->insert([

                [
                    'code' => 'COM',
                    'name' => 'Complain',
                    'name_ne' => 'Complain',
                    'created_at' => now(),
                ],
                [
                    'code' => 'ENQ',
                    'name' => 'Enquiry',
                    'name_ne' => 'Enquiry',
                    'created_at' => now(),
                ],
                [
                    'code' => 'REQ',
                    'name' => 'Request',
                    'name_ne' => 'Request',
                    'created_at' => now(),
                ],
                [
                    'code' => 'SUG',
                    'name' => 'Suggestion',
                    'name_ne' => 'Suggestion',
                    'created_at' => now(),
                ],
                [
                    'code' => 'CNR',
                    'name' => 'Complain & Request',
                    'name_ne' => 'Complain & Request',
                    'created_at' => now(),
                ],
                [
                    'code' => 'APR',
                    'name' => 'Appreciation',
                    'name_ne' => 'Appreciation',
                    'created_at' => now(),
                ],

            ]);
        }else { echo "Table is not empty, therefore NOT seeded."; }
    }
}
