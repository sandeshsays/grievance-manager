<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\SuggestionController;
use App\Http\Controllers\SubmissionsController;
use App\Http\Controllers\ActiveLocalGovtController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', function () {
    header('Location:/admin');
    exit();
})->name('login');


Route::group(['prefix' => 'admin'], function () {
    Route::get('/get_submissions', [SubmissionsController::class, 'get_submissions'])->name('get_submissions');
    Route::get('/get_asset_data', [SubmissionsController::class, 'get_asset_data'])->name('get_asset_data');
    Route::get('/admin_dashboard', [DashboardController::class, 'admin_dashboard'])->name('admin_dashboard');
    Route::get('/supervisor_dashboard', [DashboardController::class, 'supervisor_dashboard'])->name('supervisor_dashboard');
    Route::get('/active_local_govt', [ActiveLocalGovtController::class, 'index'])->name('active_local_govt');

    // Route::get('/admin_dashboard', function () {
    //     return view('admin_dashboard');
    // });
    // Route::get('/supervisor_dashboard', function () {
    //     return view('supervisor_dashboard');
    // });
});

Route::resource('suggestions', SuggestionController::class);

Route::get('/', [LanguageController::class, 'index'])->name('home');
Route::get('lan-change', [LanguageController::class, 'langChange'])->name('lan.change');
