@extends('crudbooster::admin_template')
@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Surveyed Local Governments</h3>
                </div>

                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Code</th>
                                <th>Local Government</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($local_govt as $row)
                                <tr>
                                    <td>{{ $row->local_govt_code }}</td>
                                    <td>{{ $row->local_govt }}</td>
                                </tr>
                            @endforeach
                        </tbody>

                    </table>
                    {{ $local_govt->links() }}

                </div>

            </div>
        </div>
    </div>
@endsection
