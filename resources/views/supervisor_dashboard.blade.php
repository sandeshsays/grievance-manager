@extends('crudbooster::admin_template')
@section('content')
    {{-- <div class="row">
        <div class="col-xs-12">
            <div class="col-lg-6 col-xs-6">
                <h2>Supervisor Dashboard</h2>
            </div>
            <div class="col-lg-6 col-xs-6">
                <a href="{{ route('get_submissions') }}" class="btn btn-primary pull-right">
                    Reload Submissions <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>
            </div>
        </div>
    </div> --}}

    <div class="row">

        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-map-marker"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Local Government</span>
                    <span class="info-box-number">{{ Session::get('local_government') }}</span>
                    <span class="info-box-text">{{ Session::get('district') }} , {{ Session::get('province') }}</span>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Data Collectors</span>
                    <span class="info-box-number">{{ $data_collectors_count }}</span>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-file-text-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Households</span>
                    <span class="info-box-number">{{ $data_count->count }}</span>
                </div>
            </div>
        </div>

        {{-- <div class="clearfix visible-sm-block"></div> --}}

        {{-- <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">New Members</span>
                    <span class="info-box-number">2,000</span>
                </div>
            </div>
        </div> --}}

    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Collectors List</h3>
                </div>

                <div class="box-body">
                    <table id="data_collectors" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Data Collector</th>
                                <th>Full Name</th>
                                <th>Email</th>
                                <th>Contact No.</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data_collectors as $row)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $row->username }}</td>
                                    <td>{{ $row->full_name }}</td>
                                    <td>{{ $row->email }}</td>
                                    <td>{{ $row->mobile }}</td>
                                </tr>
                            @endforeach
                        </tbody>

                    </table>
                    {{-- {{ $data_collectors->links() }} --}}

                </div>

            </div>
        </div>
    </div>
@endsection
