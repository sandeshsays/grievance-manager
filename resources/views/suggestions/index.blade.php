@extends('layout')

@section('content')

<section id="services" class="services section-bg">
    <div class="container">

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>{{ __('frontend.suggestion') }}</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('suggestions.create') }}"><i class="fa fa-plus-circle"></i> {{ __('frontend.new') }}</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered table-striped table-hover table-condensed table-responsive">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Mobile</th>
            <th>Suggestion</th>
            <th>Files</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($suggestions as $suggestion)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $suggestion->name }}</td>
            <td>{{ $suggestion->mobile }}</td>
            <td>{{ $suggestion->suggestion }}</td>
            <td><img src="/image/{{ $suggestion->image }}" width="100px"></td>
            <td>
                <form action="{{ route('suggestions.destroy',$suggestion->id) }}" method="POST">

                    <a class="btn btn-info" href="{{ route('suggestions.show',$suggestion->id) }}"><i class="fa fa-eye"></i></a>

                    <a class="btn btn-primary" href="{{ route('suggestions.edit',$suggestion->id) }}"><i class="fa fa-pencil"></i></a>

                    @csrf
                    @method('DELETE')

                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>

    {!! $suggestions->links() !!}

    </div>
</section>

@endsection
