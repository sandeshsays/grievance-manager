# Read Me

- https://github.com/bfinlay/laravel-excel-seeder

Firstly make a file at /database/seeders/example.xlsx
Then make a seeder file and run it.
php artisan make:seeder ExampleSeeder
php artisan db:seed --class=ExampleSeeder


## tables : 

### office_categories--
    code, name, name_ne, status

### offices
    code, name, name_ne, status, office_category_id, logo, description, state_id, district_id, local_govt_id, ward, street

### faqs--
    question, question_ne, answer, answer_ne, status

### sources (form-data-channels)
    source, value, color, status

### user_manuals--
    name, name_ne, manual_url, status

### form_categories--
* [Traffic Management, Waste Management, Electricity, Water Supply, Road Management]
    - code, name, name_ne, status

### form_types--
* [Complain, Enquiry, Request, Suggestion, Complain & Request, Appreciation]
    code, name, name_ne, status

### severity_types-- 
* [normal, medium, urgent, blocker]
    code, name, name_ne, status, depth

### form
    form_category_id, name, name_ne, code, form_type_id, severity_id, status, default_response, default_public_response, 
    पूर्वनिर्धारित प्रतिक्रिया (आन्तरिक/कार्यालय प्रयोजनको लागी), पूर्वनिर्धारित प्रतिक्रिया (गुनासोकर्ता/सार्वजनिकका लागि)    

### complaint_status--
* [Not Seen, Seen/Pending, Processing, Solved, Un-Assigned, Re-Opened, Re-Assigned/Unseen, Closed, Incomplete/Fake/Unrelated, Informative?, To Follow Up?, To Follow Up (Active)?]
    code, name, name_ne, status, depth

### complaint_sources--
* [National Call, International Call, Facebook, Twitter, Sms, Skype, Viber, Imo, Mobile, Website, Mail, Whatsapp, Landline ]
    code, name, name_ne, status, depth

### complaints
    step1 > select complaint type

### notices--
    title, image, attachment, content, expiry_date 

### suggestions
    name, mobile, suggestion, files 

### privacy_policies
    title, description, title_np, description_np, status

## migration
- php artisan make:migration create_abc_table
- php artisan migrate

## seeding
- php artisan make:seeder SeverityTypesSeeder
- php artisan db:seed --class=SeverityTypesSeeder
