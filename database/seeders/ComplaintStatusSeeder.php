<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ComplaintStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(DB::table('complaint_status')->count() == 0){
            DB::table('complaint_status')->insert([

                [
                    'code' => 'NOT',
                    'name' => 'Not Seen',
                    'name_ne' => 'Not Seen',
                    'depth' => 1,
                    'created_at' => now(),
                ],
                [
                    'code' => 'SEE',
                    'name' => 'Seen/Pending',
                    'name_ne' => 'Seen/Pending',
                    'depth' => 2,
                    'created_at' => now(),
                ],
                [
                    'code' => 'PRO',
                    'name' => 'Processing',
                    'name_ne' => 'Processing',
                    'depth' => 3,
                    'created_at' => now(),
                ],
                [
                    'code' => 'SOL',
                    'name' => 'Solved',
                    'name_ne' => 'Solved',
                    'depth' => 4,
                    'created_at' => now(),
                ],
                [
                    'code' => 'UN',
                    'name' => 'Un-Assigned',
                    'name_ne' => 'Un-Assigned',
                    'depth' => 5,
                    'created_at' => now(),
                ],
                [
                    'code' => 'RO',
                    'name' => 'Re-Opened',
                    'name_ne' => 'Re-Opened',
                    'depth' => 6,
                    'created_at' => now(),
                ],
                [
                    'code' => 'RA',
                    'name' => 'Re-Assigned/Unseen',
                    'name_ne' => 'Re-Assigned/Unseen',
                    'depth' => 7,
                    'created_at' => now(),
                ],
                [
                    'code' => 'CLO',
                    'name' => 'Closed',
                    'name_ne' => 'Closed',
                    'depth' => 8,
                    'created_at' => now(),
                ],
                [
                    'code' => 'FA',
                    'name' => 'Incomplete/Fake/Unrelated',
                    'name_ne' => 'Incomplete/Fake/Unrelated',
                    'depth' => 9,
                    'created_at' => now(),
                ],

            ]);
        }else { echo "Table is not empty, therefore NOT seeded."; }
    }
}
