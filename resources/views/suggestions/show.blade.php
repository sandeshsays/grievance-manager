@extends('layout')

@section('content')

<section id="services" class="services section-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2> Show Suggestion</h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('suggestions.index') }}"><i class="fa fa-arrow-left"></i>  {{ __('frontend.back') }}</a>
                </div>
            </div>
            <ul class="list-group">
                <li class="list-group-item"><strong>Name:</strong>
                    {{ $suggestion->name }}</li>
                <li class="list-group-item"><strong>Mobile:</strong>
                    {{ $suggestion->mobile }}</li>
                <li class="list-group-item"><strong>Suggestion:</strong>
                    {{ $suggestion->suggestion }}</li>
                <li class="list-group-item"><strong>Image:</strong>
                    <img src="/image/{{ $suggestion->image }}" width="500px"></li>
            </ul>
        </div>
    </div>

</section>

@endsection
