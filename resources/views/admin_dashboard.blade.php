@extends('crudbooster::admin_template')
@section('content')

    <div class="row">

        <div class="col-lg-2 col-xs-6">

            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{ $surveyed_local_govt_count }}</h3>
                    <p>complain</p>
                </div>
                <div class="icon">
                    <i class="ion ion-map"></i>
                </div>
                <a href={{ CRUDBooster::adminPath('active_local_govt') }} class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-2 col-xs-6">

            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>{{ $supervisors_count }}</h3>
                    <p>enquiry</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person"></i>
                </div>
                <a href={{ CRUDBooster::adminPath('supervisors') }} class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-2 col-xs-6">

            <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{ $data_collectors }}</h3>
                    <p>request</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person"></i>
                </div>
                <a href="{{ CRUDBooster::adminPath('data_collector') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-2 col-xs-6">

            <div class="small-box bg-purple">
                <div class="inner">
                    <h3>{{ $collected_data }}</h3>
                    <p>suggestion</p>
                </div>
                <div class="icon">
                    <i class="ion ion-document"></i>
                </div>
                <a href="{{ route('get_submissions') }}" class="small-box-footer">Update data <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-2 col-xs-6">

            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{ $collected_data }}</h3>
                    <p>complain & request</p>
                </div>
                <div class="icon">
                    <i class="ion ion-document"></i>
                </div>
                <a href="{{ route('get_submissions') }}" class="small-box-footer">Update data <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-2 col-xs-6">

            <div class="small-box bg-primary">
                <div class="inner">
                    <h3>{{ $collected_data }}</h3>
                    <p>appreciation</p>
                </div>
                <div class="icon">
                    <i class="ion ion-document"></i>
                </div>
                <a href="{{ route('get_submissions') }}" class="small-box-footer">Update data <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Complaints</h3>
                </div>

                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Complaints</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>

                    </table>
                    {{-- {{ $submissions_summary->links() }} --}}

                </div>

            </div>
        </div>
    </div>
@endsection
