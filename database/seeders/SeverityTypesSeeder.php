<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SeverityTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(DB::table('severity_types')->count() == 0){
            DB::table('severity_types')->insert([

                [
                    'code' => 'N',
                    'name' => 'normal',
                    'name_ne' => 'normal',
                    'depth' => 1,
                    'created_at' => now(),
                ],
                [
                    'code' => 'M',
                    'name' => 'medium',
                    'name_ne' => 'medium',
                    'depth' => 2,
                    'created_at' => now(),
                ],
                [
                    'code' => 'U',
                    'name' => 'urgent',
                    'name_ne' => 'urgent',
                    'depth' => 3,
                    'created_at' => now(),
                ],
                [
                    'code' => 'B',
                    'name' => 'blocker',
                    'name_ne' => 'blocker',
                    'depth' => 4,
                    'created_at' => now(),
                ],

            ]);
        }else { echo "Table is not empty, therefore NOT seeded."; }
    }
}
