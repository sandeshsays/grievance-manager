<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocalGovernmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('local_government', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('name_np');
            $table->string('name_en')->nullable();
            $table->foreignId('local_government_type_id')->nullable()->constrained('local_government_type');
            $table->foreignId('district_id')->nullable()->constrained('district');
            $table->foreignId('province_id')->nullable()->constrained('province');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('local_government');
    }
}
