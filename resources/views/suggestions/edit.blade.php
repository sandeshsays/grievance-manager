@extends('layout')

@section('content')

<section id="services" class="services section-bg">
    <div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Suggestion</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('suggestions.index') }}"><i class="fa fa-arrow-left"></i>  {{ __('frontend.back') }}</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('suggestions.update',$suggestion->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')

         <div class="row">
            {{-- <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="name" value="{{ $suggestion->name }}" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Mobile:</strong>
                    <input type="text" class="form-control" name="mobile" placeholder="Mobile">
                </div>
            </div> --}}
            <div class="form-group col-md-6 mt-3 mt-md-0">
                <strong>Name:</strong>
                <input type="text" name="name" class="form-control" value="{{ $suggestion->name }}">
            </div>
            <div class="form-group col-md-6 mt-3 mt-md-0">
                <strong>Mobile Number:</strong>
                <input type="text" class="form-control" name="mobile" value="{{ $suggestion->mobile }}">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Suggestion:</strong>
                    <textarea class="form-control" name="suggestion">{{ $suggestion->suggestion }}</textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>File:</strong>
                    <input type="file" name="files" class="form-control" placeholder="image">
                    <img src="/image/{{ $suggestion->files }}" width="300px">
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>

    </form>
    </div>
</section>

<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
       $('.ckeditor').ckeditor();
    });
</script>
@endsection
