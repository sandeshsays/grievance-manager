<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FormCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(DB::table('form_categories')->count() == 0){
            DB::table('form_categories')->insert([

                [
                    'code' => 'T',
                    'name' => 'Traffic Management',
                    'name_ne' => 'Traffic Management',
                    'created_at' => now(),
                ],
                [
                    'code' => 'W',
                    'name' => 'Waste Management',
                    'name_ne' => 'Waste Management',
                    'created_at' => now(),
                ],
                [
                    'code' => 'E',
                    'name' => 'Electricity',
                    'name_ne' => 'Electricity',
                    'created_at' => now(),
                ],
                [
                    'code' => 'W',
                    'name' => 'Water Supply',
                    'name_ne' => 'Water Supply',
                    'created_at' => now(),
                ],
                [
                    'code' => 'R',
                    'name' => 'Road Management',
                    'name_ne' => 'Road Management',
                    'created_at' => now(),
                ],

            ]);
        }else { echo "Table is not empty, therefore NOT seeded."; }
    }
}
